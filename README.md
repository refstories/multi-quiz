# Hello World Story
**To read**: [https://epa.ms/hello-world-story]

**Estimated reading time**: 10 minutes

## Story Outline
This story shows capabilities of Refactoring Stories UI and
examples of extended markdown you could use for story writing.

Refactoring Stories: [https://rs.delivery.epam.com]

Read more about syntax [here](https://kb.epam.com/display/ENGX/Author%27s+Guide#Author'sGuide-Storysyntaxguide).

## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: template, capabilities, exclude